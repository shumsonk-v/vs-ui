import { Directive, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../common/index';
import * as layout from '../common/layout/layout.actions';

declare var $: any;

@Directive({ selector: '[sidenav-toggle]' })
export class SidenavToggleDirective implements OnInit {

  constructor(private _el: ElementRef,
    private _store: Store<fromRoot.AppState>) { }

  public ngOnInit() {
    $(this._el.nativeElement).on('click.sidenav', () => {
      if ($('sidenav').hasClass('closing')) {
        return false;
      }

      const toClose: boolean = $('sidenav').hasClass('expanded');
      if (toClose) {
        $('sidenav').addClass('closing');
      } else {
        $('sidenav').addClass('expanded');
      }
      this._store.dispatch(new layout.ToggleSidenavAction(!toClose));
    });
  }
}
