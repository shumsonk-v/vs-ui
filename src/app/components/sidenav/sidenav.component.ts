import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../common/index';
import * as layout from '../../common/layout/layout.actions';

declare var $: any;
declare var _: any;

@Component({
  selector: 'sidenav',
  templateUrl: 'sidenav.component.html'
})

export class SidenavComponent implements OnInit {
  @ViewChild('navElement') navElement: ElementRef;
  constructor(private _el: ElementRef,
    private _store: Store<fromRoot.AppState>) { }

  public ngOnInit() {
    $(this.navElement.nativeElement).on('webkitAnimationEnd oanimationend msAnimationEnd animationend', () => {
      if ($(this._el.nativeElement).hasClass('closing')) {
        $(this._el.nativeElement).removeClass('expanded closing');
      }
    });

    const resizeHandler = _.debounce(() => {
      if ($(this._el.nativeElement).hasClass('closing') ||
        $(this._el.nativeElement).hasClass('expanded')) {
        $(this._el.nativeElement).removeClass('expanded closing');
      }
    }, 300);

    $(window).on('resize.sidenav', resizeHandler);
  }
}
