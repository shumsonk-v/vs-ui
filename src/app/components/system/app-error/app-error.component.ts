import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

interface IAppError {
  code?: number;
  title: string;
  descrition?: string;
}

@Component({
  selector: 'app-error',
  templateUrl: './app-error.component.html'
})
export class AppErrorComponent implements OnInit {

  public info: IAppError = null;

  constructor(private _acRoute: ActivatedRoute) { }

  public ngOnInit() {
    if (this._acRoute.snapshot.data) {
      this.info = this._acRoute.snapshot.data['error'] || null;
    }
  }

}
