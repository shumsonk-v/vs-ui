import { GuidelineComponent } from './guideline.component';
import { GuidelineColorsComponent } from './components/colors/colors.component';
import { GuidelineIntroductionComponent } from './components/introduction/introduction.component';
import { GuidelineTypoComponent } from './components/typography/typography.component';
import { GuidelineButtonsComponent } from './components/buttons/buttons.component';
import { GuidelineButtonGroupComponent } from './components/button-group/button-group.component';
import { GuidelineAlertComponent } from './components/alert/alert.component';
import { GuidelineBadgeComponent } from './components/badge/badge.component';
import { GuidelineBreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { GuidelineCardComponent } from './components/card/card.component';
import { GuidelineFormComponent } from './components/form/form.component';
import { GuidelineInputGroupComponent } from './components/input-group/input-group.component';
import { GuidelineJumbotronComponent } from './components/jumbotron/jumbotron.component';
import { GuidelineListGroupComponent } from './components/list-group/list-group.component';
import { GuidelineModalComponent } from './components/modal/modal.component';

export const routes = [
  {
    path: '',
    component: GuidelineComponent,
    children: [
      {
        path: '',
        redirectTo: 'colors'
      },
      {
        path: 'colors',
        component: GuidelineColorsComponent
      },
      {
        path: 'typography',
        component: GuidelineTypoComponent
      },
      {
        path: 'buttons',
        component: GuidelineButtonsComponent
      },
      {
        path: 'button-group',
        component: GuidelineButtonGroupComponent
      },
      {
        path: 'alert',
        component: GuidelineAlertComponent
      },
      {
        path: 'badge',
        component: GuidelineBadgeComponent
      },
      {
        path: 'breadcrumb',
        component: GuidelineBreadcrumbComponent
      },
      {
        path: 'card',
        component: GuidelineCardComponent
      },
      {
        path: 'form',
        component: GuidelineFormComponent
      },
      {
        path: 'input-group',
        component: GuidelineInputGroupComponent
      },
      {
        path: 'jumbotron',
        component: GuidelineJumbotronComponent
      },
      {
        path: 'list-group',
        component: GuidelineListGroupComponent
      },
      {
        path: 'modal',
        component: GuidelineModalComponent
      }
    ]
  }
];
