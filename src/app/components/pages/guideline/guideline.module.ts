import { NgModule } from '@angular/core';
import { GuidelineComponent } from './guideline.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { routes } from './guideline.routes';
import { ModalModule } from 'ngx-bootstrap/modal';
import { GuidelineColorsComponent } from './components/colors/colors.component';
import { GuidelineIntroductionComponent } from './components/introduction/introduction.component';
import { ColorCodeBoxDirective } from './components/colors/color-code-box.directive';
import { GuidelineTypoComponent } from './components/typography/typography.component';
import { GuidelineButtonsComponent } from './components/buttons/buttons.component';
import { GuidelineButtonGroupComponent } from './components/button-group/button-group.component';
import { GuidelineAlertComponent } from './components/alert/alert.component';
import { VsUiModule } from '../../../modules/vs-ui/index';
import { GuidelineBadgeComponent } from './components/badge/badge.component';
import { GuidelineBreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { GuidelineCardComponent } from './components/card/card.component';
import { GuidelineFormComponent } from './components/form/form.component';
import { GuidelineInputGroupComponent } from './components/input-group/input-group.component';
import { GuidelineJumbotronComponent } from './components/jumbotron/jumbotron.component';
import { GuidelineListGroupComponent } from './components/list-group/list-group.component';
import { GuidelineModalComponent } from './components/modal/modal.component';

const MODULES = [
  CommonModule,
  FormsModule,
  RouterModule.forChild(routes),
  VsUiModule
];

const COMPONENTS = [
  GuidelineComponent,
  GuidelineColorsComponent,
  GuidelineIntroductionComponent,
  GuidelineTypoComponent,
  GuidelineButtonsComponent,
  GuidelineButtonGroupComponent,
  GuidelineAlertComponent,
  GuidelineBadgeComponent,
  GuidelineBreadcrumbComponent,
  GuidelineCardComponent,
  GuidelineFormComponent,
  GuidelineInputGroupComponent,
  GuidelineJumbotronComponent,
  GuidelineListGroupComponent,
  GuidelineModalComponent
];

const PRIVATE_DIRECTIVES = [
  ColorCodeBoxDirective
];

@NgModule({
  imports: [
    ...MODULES,
    ModalModule.forRoot()
  ],
  exports: [],
  declarations: [
    ...COMPONENTS,
    ...PRIVATE_DIRECTIVES
  ],
  providers: []
})
export class GuidelineModule {
  public static routes = routes;
}
