import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GuidelineColorsComponent } from './components/colors/colors.component';
import { GuidelineIntroductionComponent } from './components/introduction/introduction.component';
import { GuidelineTypoComponent } from './components/typography/typography.component';
import { GuidelineButtonsComponent } from './components/buttons/buttons.component';
import { GuidelineButtonGroupComponent } from './components/button-group/button-group.component';
import { Subscription } from 'rxjs/Subscription';
import { GuidelineAlertComponent } from './components/alert/alert.component';
import { GuidelineBadgeComponent } from './components/badge/badge.component';
import { GuidelineBreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { GuidelineCardComponent } from './components/card/card.component';
import { GuidelineFormComponent } from './components/form/form.component';
import { GuidelineInputGroupComponent } from './components/input-group/input-group.component';
import { GuidelineJumbotronComponent } from './components/jumbotron/jumbotron.component';
import { GuidelineListGroupComponent } from './components/list-group/list-group.component';
import { GuidelineModalComponent } from './components/modal/modal.component';


@Component({
  selector: 'guideline',
  templateUrl: 'guideline.component.html',
  entryComponents: [
    GuidelineColorsComponent,
    GuidelineIntroductionComponent,
    GuidelineTypoComponent,
    GuidelineButtonsComponent,
    GuidelineButtonGroupComponent,
    GuidelineAlertComponent,
    GuidelineBadgeComponent,
    GuidelineBreadcrumbComponent,
    GuidelineCardComponent,
    GuidelineFormComponent,
    GuidelineInputGroupComponent,
    GuidelineJumbotronComponent,
    GuidelineListGroupComponent,
    GuidelineModalComponent
  ]
})
export class GuidelineComponent implements OnInit {
  public guidelineLinkList = [
    {
      url: '/guideline/alert',
      text: 'Alert'
    },
    {
      url: '/guideline/badge',
      text: 'Badge'
    },
    {
      url: '/guideline/breadcrumb',
      text: 'Breadcrumb'
    },
    {
      url: '/guideline/buttons',
      text: 'Buttons'
    },
    {
      url: '/guideline/button-group',
      text: 'Button Group'
    },
    {
      url: '/guideline/colors',
      text: 'Colors'
    },
    {
      url: '/guideline/card',
      text: 'Card'
    },
    {
      url: '/guideline/form',
      text: 'Form'
    },
    {
      url: '/guideline/input-group',
      text: 'Input Group'
    },
    {
      url: '/guideline/jumbotron',
      text: 'Jumbotron'
    },
    {
      url: '/guideline/list-group',
      text: 'List Group'
    },
    {
      url: '/guideline/modal',
      text: 'Modal'
    },
    {
      url: '/guideline/typography',
      text: 'Typography'
    }
  ];
  private _currentRoute = '';
  private _currentRouteUrl = '';

  constructor(private _router: Router, private _acRoute: ActivatedRoute) {
    const matchedUrl = this.guidelineLinkList.filter((item) => {
      return item.url === this._router.url;
    });
    if (matchedUrl.length) {
      this._currentRoute = matchedUrl[0].text;
      this._currentRouteUrl = matchedUrl[0].url;
    }
  }

  public ngOnInit() { }

  private navigate(urlObject) {
    this._currentRoute = urlObject.text;
    this._currentRouteUrl = urlObject.url;
    this._router.navigate([urlObject.url]);
  }
}
