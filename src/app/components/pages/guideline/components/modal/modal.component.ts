import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'guildeline-modal',
  templateUrl: 'modal.component.html'
})

export class GuidelineModalComponent implements OnInit {
  public modalRef: BsModalRef;
  public centeredModalRef: BsModalRef;
  public fsModalRef: BsModalRef;
  public expandedMode = 'expanded';
  constructor(private _modalSvc: BsModalService) { }

  ngOnInit() { }

  public openFirstModal(template: TemplateRef<any>) {
    const config = {
      backdrop: false,
      ignoreBackdropClick: false
    };
    this.modalRef = this._modalSvc.show(template, config);
  }

  public openCenteredModal(template: TemplateRef<any>) {
    const config = {
      class: 'modal-dialog-centered',
      backdrop: false,
      ignoreBackdropClick: false
    };
    this.centeredModalRef = this._modalSvc.show(template, config);
  }

  public openFullscreenModal(template: TemplateRef<any>) {
    const config = {
      class: `modal-dialog-${this.expandedMode}`,
      backdrop: false,
      ignoreBackdropClick: false
    };
    this.fsModalRef = this._modalSvc.show(template, config);
  }
}
