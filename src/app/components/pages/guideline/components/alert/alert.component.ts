import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'guideline-alert',
  templateUrl: 'alert.component.html'
})

export class GuidelineAlertComponent implements OnInit {
  private _showCustom = false;
  public displayCustomAlert = true;
  constructor() { }

  public ngOnInit() { }

  showCustomAlert() {
    this._showCustom = true;
  }
}
