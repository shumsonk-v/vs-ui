import { Directive, AfterViewInit, ElementRef } from '@angular/core';

declare var $: any;

@Directive({ selector: '[color-code-box]' })
export class ColorCodeBoxDirective implements AfterViewInit {
  constructor(private _el: ElementRef) { }

  public ngAfterViewInit() {
    setTimeout(() => {
      if ((<HTMLElement> this._el.nativeElement)) {
        let bgColor = $(this._el.nativeElement).css('background-color');
        if (bgColor) {
          const bgColorRgbArr = bgColor.replace(/(rgb)a*/g, '').replace(/[\(\)\s]/g, '').split(',');

          if (bgColorRgbArr.length === 3) {
            bgColor = `#${this.toHex(bgColorRgbArr[0])}${this.toHex(bgColorRgbArr[1])}${this.toHex(bgColorRgbArr[2])}`;
            $(this._el.nativeElement).append(`<div class="color-code">${bgColor}</div>`);
          }
        }
      }
    }, 1);
  }

  private toHex(n) {
    n = parseInt(n, 10);
    if (isNaN(n)) {
      return '00';
    }
    n = Math.max(0, Math.min(n, 255));
    return '0123456789ABCDEF'.charAt((n - n % 16) / 16) + '0123456789ABCDEF'.charAt(n % 16);
  }
}
