import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'guideline-colors',
  templateUrl: 'colors.component.html'
})
export class GuidelineColorsComponent implements OnInit {
  public mainColorList = [
    'primary', 'secondary', 'danger', 'success', 'warning', 'info'
  ];
  public statusColorList = [
    {
      name: 'focus',
      description: 'Used as a border/background color for focus state'
    },
    {
      name: 'active',
      description: 'Used as a border/background color for active state'
    },
    {
      name: 'disabled',
      description: 'Used as a background color for disable state'
    }
  ];

  constructor() { }

  public ngOnInit() { }
}
