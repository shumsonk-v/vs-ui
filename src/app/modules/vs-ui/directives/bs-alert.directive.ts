import { Directive, ElementRef, AfterViewInit, OnInit, Output, EventEmitter } from '@angular/core';

declare var $: any;

@Directive({
  selector: '.alert-dismissible'
})
export class BsAlertDirective implements OnInit {
  @Output() alertClosed = new EventEmitter<any>();
  constructor(private _el: ElementRef) { }

  public ngOnInit() {
    const alertElem = $(this._el.nativeElement);
    const newCloseBtn = $('<button></button>').addClass('close-alert');
    alertElem.removeClass('fade');
    alertElem.find('.close').remove();
    alertElem.append(newCloseBtn);
    alertElem.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', (e) => {
      if (alertElem.hasClass('closing')) {
        alertElem.alert('hide');
        this.alertClosed.emit(true);
      }
    });
    $(this._el.nativeElement).on('click.bs-alert', '.close-alert', (e) => {
      alertElem.addClass('closing');
    });
  }
}
