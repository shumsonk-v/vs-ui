import { Directive, ElementRef, OnInit, Input } from '@angular/core';

declare var $: any;

const TOGGLE_TYPE = {
  CHECKBOX: 'checkbox',
  RADIO: 'radio',
  SWITCH: 'switch',
  NONE: 'none'
};

// const svgCheckIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
//     <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/></svg>`;
const svgCheckIcon = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
   version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 18 18" style="enable-background:new 0 0 18 18;"
   xml:space="preserve"><path d="M6.4,12.7L2.8,9l-1.2,1.2l4.9,4.9L16.8,4.7l-1.2-1.2L6.4,12.7z"/></svg>`;

@Directive({ selector: '.form-check' })
export class BsToggleDirective implements OnInit {
  @Input() toggleType = ''; // accept only 3 values, checkbox, radio and switch
  private _elemType = TOGGLE_TYPE.NONE;
  private _elem: HTMLElement;
  private _toggleInput;

  constructor(private _el: ElementRef) {
    this._elem = <HTMLElement> this._el.nativeElement;
  }

  public ngOnInit() {
    this.checkElementType();
  }

  private checkElementType() {
    this._toggleInput = $(this._elem).find('input');
    if (this.toggleType) {
      this._elemType = this.toggleType;
    } else {
      if (this._toggleInput.attr('type') === 'checkbox') {
        this._elemType = TOGGLE_TYPE.CHECKBOX;
      } else if (this._toggleInput.attr('type') === 'radio') {
        this._elemType = TOGGLE_TYPE.RADIO;
      }
    }

    // Check if it's a switch but no checkbox input type exist
    if (this._elemType === 'switch' && !$(this._elem).find('[type="checkbox"]').length) {
      this._elemType = TOGGLE_TYPE.NONE;
    }

    if (this._elemType !== TOGGLE_TYPE.NONE && this._toggleInput.length) {
      this.helperBuilder();
    } else {
      console.warn('No toggleType specified or no checkbox/radio input in your .form-check div');
    }
  }

  private helperBuilder() {
    let helperElem = $(`<span class="toggle-helper">${this._elemType === TOGGLE_TYPE.CHECKBOX ? svgCheckIcon : ''}</span>`);
    helperElem.insertAfter(this._toggleInput);
    $(this._elem).addClass(`toggle-box toggle-${this._elemType}`);
    $(this._toggleInput).addClass('toggle-elem');

    helperElem = $(this._toggleInput).next('.toggle-helper');
    helperElem.on('click.vs-toggle', (e) => {
      $(this._toggleInput).trigger('click').focus();
    });
  }
}
