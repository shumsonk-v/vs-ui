import { Injectable, ViewContainerRef, ComponentFactory, ComponentRef } from '@angular/core';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { Observable } from 'rxjs/Observable';
import { ISnackbarOption } from '../types';

declare var $: any;

export interface IVsUiComponentFactories {
  snackbar?: ComponentFactory<SnackbarComponent>;
}

export interface IVsUiGlobalServiceInitParam {
  uiContainer: ViewContainerRef;
  componentFactoryList: IVsUiComponentFactories;
}

@Injectable()
export class VsUiGlobalService {
  private _settings: IVsUiGlobalServiceInitParam = null;
  private _collection: ComponentRef<any>[] = [];

  constructor() { }

  public init(settings: IVsUiGlobalServiceInitParam) {
    this._settings = settings;
  }

  public getSettings(): IVsUiGlobalServiceInitParam {
    return this._settings;
  }

  // Snackbar
  public openSnackbar(barSetting: ISnackbarOption) {
    if (this._settings.uiContainer) {
      const newComponent = this._settings.uiContainer.createComponent(this._settings.componentFactoryList.snackbar);

      if (newComponent && barSetting.message) {
        const barRef: any = newComponent.instance;
        barRef.barSetting = barSetting;
        this._collection.push(newComponent);
      }
    } else {
      console.error('No view container assigned');
    }
  }

  public closeSnackbar() {
    $(document).off('click.snack-bar');
    setTimeout(() => {
      this.destroyComponent('SnackbarComponent').subscribe((result) => {
        $('.snack-bar').remove();
      });
    }, 400);
  }

  private destroyComponent(target: string | number): Observable<any> {
    return Observable.create((observer) => {
      let result = false;
      if (typeof target === 'string') {
        let idx = -1;
        const targetComponents = this._collection.filter((cmp) => {
          idx += 1;
          return cmp.componentType.name === target;
        });
        if (targetComponents.length) {
          targetComponents[0].destroy();
          result = true;
        }
      } else if (typeof target === 'number') {
        if (this._collection[target]) {
          this._collection[0].destroy();
          result = true;
        }
      }

      observer.next(result);
      observer.complete();
    });
  }
}
