import { Component, OnInit, AfterViewInit, ElementRef, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApplicationRef } from '@angular/core/src/application_ref';
import { VsUiGlobalService } from '../services/vs-ui-global.service';

declare var $: any;

export const SNACKBAR_TYPE = {
  NORMAL: 'normal',
  DANGER: 'danger',
  SUCCESS: 'success',
  WARNING: 'warning'
};

export interface ISnackbarOption {
  message: string;
  type?: string;
  autoClose?: boolean;
  duration?: number;
}


@Component({
  selector: 'snackbar',
  templateUrl: 'snackbar.component.html'
})

export class SnackbarComponent implements OnInit, AfterViewInit {
  public barSetting: ISnackbarOption = {
    message: '',
    autoClose: true,
    duration: 3000
  };
  private _message = '';
  private _type: string = SNACKBAR_TYPE.NORMAL;
  private _show = false;
  private _bar: any;

  constructor(private el: ElementRef, private _vsuiSvc: VsUiGlobalService,
    private _zone: NgZone, private _translateSvc: TranslateService) {
    this._bar = $(el.nativeElement);
  }

  //
  public ngOnInit() {
    $(document).off('click.snack-bar');
    this._message = this.barSetting.message;
    this._type = this.barSetting.type || SNACKBAR_TYPE.NORMAL;
  }

  public ngAfterViewInit() {
    setTimeout(() => {
      this._translateSvc.get(this._message).subscribe((res) => {
        this._show = true;

        if (this.barSetting.autoClose === undefined) {
          this.barSetting.autoClose = true;
        }
        if (!this.barSetting.duration) {
          this.barSetting.duration = 4000;
        }

        if (this.barSetting.autoClose) {
          setTimeout(() => {
            this.close();
          }, this.barSetting.duration || 3000);
        }

        $(document).on('click.snack-bar', (e) => {
          if (!$(e.target).hasClass('.snack-bar') &&
            $(e.target).closest('.snack-bar').length === 0) {
            this._zone.run(() => {
              this.close();
            });
          }
        });
      });
    }, 1);
  }

  private close() {
    this._show = false;
    this._vsuiSvc.closeSnackbar();
  }
}
