import { NgModule } from '@angular/core';
import { SnackbarComponent } from './snackbar/snackbar.component';
import { VsUiGlobalService } from './services/vs-ui-global.service';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { BsAlertDirective } from './directives/bs-alert.directive';
import { CommonModule } from '@angular/common';
import { BsToggleDirective } from './directives/bs-toggle.directive';


export const VS_UI_COMPONENTS = [
  SnackbarComponent
];

export const VS_UI_DIRECTIVES = [
  BsAlertDirective,
  BsToggleDirective
];

const SERVICES = [
  VsUiGlobalService
];

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [
    ...VS_UI_COMPONENTS,
    ...VS_UI_DIRECTIVES
  ],
  declarations: [
    ...VS_UI_COMPONENTS,
    ...VS_UI_DIRECTIVES
  ],
  providers: [
    ...SERVICES
  ]
})
export class VsUiModule {
  // static forRoot() {
  //   return {
  //     ngModule: VsUiModule,
  //     providers: [
  //       ...SERVICES
  //     ]
  //   };
  // }
}
