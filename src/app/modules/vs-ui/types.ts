export const SNACKBAR_TYPE = {
  NORMAL: 'normal',
  DANGER: 'danger',
  SUCCESS: 'success',
  WARNING: 'warning'
};

export interface ISnackbarOption {
  message: string;
  type?: string;
  autoClose?: boolean;
  duration?: number;
}