import { Injectable } from '@angular/core';

// custom header interface
export interface ICustomHeader {
  name: string;
  value: string;
}

// Options for http interceptor custom configuration
export interface IHttpInterceptorOptions {
  ignoredList?: any[];
  customHeaders?: ICustomHeader[];
}

@Injectable()
export class HttpManagerService {

  private _options: IHttpInterceptorOptions = {};

  constructor() { }

  public getHttpInterceptorConfig(): IHttpInterceptorOptions {
    return this._options;
  }

  public setHttpInterceptorConfig(options: IHttpInterceptorOptions) {
    this._options = Object.assign({}, this._options, options);
  }
}
