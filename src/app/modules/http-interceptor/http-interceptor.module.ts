import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpInterceptorService } from './http-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpManagerService } from './http-manager.service';

// To make http interceptor custom config to match your app spec,
// use HttpManagerService and call method 'setHttpInterceptorConfig(options)'
// and parse your configuration (in IHttpInterceptorOptions interface) as a parameter

// The interceptor will obtain the settings from there and use it in an interceptor process

@NgModule({
  imports: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
})
export class HttpInterceptorModule {
  /**
   * Use this method in your root module to provide the your exported services
   * @param none
   * @returns {ModuleWithProviders}
   */
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: HttpInterceptorModule,
      providers: [
        HttpManagerService
      ]
    };
  }

  /**
   * Use this method in your other (non root) modules to import the directive/pipe
   * @param none
   * @returns {ModuleWithProviders}
   */
  static forChild(): ModuleWithProviders {
    return {
      ngModule: HttpInterceptorModule,
      providers: [
        HttpManagerService
      ]
    };
  }
}
