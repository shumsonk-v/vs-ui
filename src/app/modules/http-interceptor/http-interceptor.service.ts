/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { IHttpInterceptorOptions, HttpManagerService } from './http-manager.service';
/* tslint:enable */

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(private _httpMgrSvc: HttpManagerService) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const globalOptions: IHttpInterceptorOptions = this._httpMgrSvc.getHttpInterceptorConfig();

    console.log('%c[DEBUG] - %cOh, hey! You\'re entering HTTP interceptor with requested url: %c' + req.url,
      'color: red;',
      'color: blue;',
      'color: #343434; font-weight: bold;');

    // Check if it's in ignored list
    if (globalOptions.ignoredList && globalOptions.ignoredList.length > 0) {
      const matchedUrls = globalOptions.ignoredList.filter((itm: any) => {
        const urlCheckPattern = new RegExp(`^(${itm})`, 'gi');
        return urlCheckPattern.test(req.url);
      });

      if (matchedUrls.length) {
        return next.handle(req);
      }
    }

    // Apply custom request headers
    const customHeaders: any = {};
    if (globalOptions.hasOwnProperty('customHeaders') &&
      globalOptions.customHeaders.length > 0) {
      for (const hdr of globalOptions.customHeaders) {
        customHeaders[hdr.name] = hdr.value;
      }
    }

    const dupReq = req.clone({
      setHeaders: customHeaders
    });
    return next.handle(dupReq).do((event) => {
      // if (event instanceof HttpResponse) {
      //   console.log('OK obtained!');
      // }
    }, (err) => {
      // if (err instanceof HttpErrorResponse) {
      //   console.error('Error obtained!');
      // }
    });
  }
}
