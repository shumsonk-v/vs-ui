import { NgModule, ModuleWithProviders } from '@angular/core';
import { SidenavToggleDirective } from '../directives/sidenav-toggle.directive';
import { LogService } from '../services/log.service';
import { VsUiModule } from './vs-ui/index';

const DIRECTIVES = [
  SidenavToggleDirective
];

const SERVICES = [
  LogService
];

@NgModule({
  imports: [],
  exports: [
    ...DIRECTIVES
  ],
  declarations: [
    ...DIRECTIVES
  ],
  providers: [
    ...SERVICES
  ]
})
export class SharedModule {
  //
}
