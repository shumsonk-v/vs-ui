import { Component, ViewChild, ViewContainerRef, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, NavigationEnd, ActivatedRoute, NavigationStart, NavigationCancel, ActivationEnd } from '@angular/router';
import { APP_SETTINGS } from './settings';
import { AppService } from './services/app.service';
import 'rxjs/add/operator/map';

// Redux
import * as fromRoot from './common/index';
import * as layout from './common/layout/layout.actions';

// VS-UI
import { VsUiGlobalService, IVsUiGlobalServiceInitParam } from './modules/vs-ui/services/vs-ui-global.service';
import { SnackbarComponent } from './modules/vs-ui/snackbar/snackbar.component';

// ** Entry components from VS UI are required to be added at metadata to create component factory
// See 'IVsUiGlobalServiceInitParam' interface for required component
// Mostly you don't have to change anything about this **
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  entryComponents: [
    SnackbarComponent
  ]
})
export class AppComponent implements OnInit {
  @ViewChild('vsUiContainer', {
    read: ViewContainerRef
  })
  public vsUiContainer: ViewContainerRef;

  public isErrorPage = true;
  public appLoading = true;

  constructor(
    private _router: Router,
    private _acRoute: ActivatedRoute,
    private _resolver: ComponentFactoryResolver,
    private _app: AppService,
    private _store: Store<fromRoot.AppState>,
    private _vsUiSvc: VsUiGlobalService
  ) {
    this._router.events.subscribe((e) => {
      if (e instanceof NavigationStart) {
        this.appLoading = true;
      }
      if (e instanceof NavigationEnd) {
        this._app.setPageMetaFromRouteData();
      }
      if (e instanceof ActivationEnd) {
        this.appLoading = false;
        this.isErrorPage = e.snapshot.data['error'] || false;
      }
    });
  }

  public ngOnInit() {
    const vsUiSettings: IVsUiGlobalServiceInitParam = {
      uiContainer: this.vsUiContainer,
      componentFactoryList: {
        snackbar: this._resolver.resolveComponentFactory(SnackbarComponent)
      }
    };
    this._vsUiSvc.init(vsUiSettings);
  }
}
