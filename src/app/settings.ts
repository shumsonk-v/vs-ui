export const APP_SETTINGS = {
  DEFAULT_TITLE: 'VS UI',
  AUTH_TOKEN_STORAGE_NAME: '__app_auth_token__'
};
