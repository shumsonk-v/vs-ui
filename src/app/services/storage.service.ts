import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  private _storageType = sessionStorage;

  constructor() { }

  public set(key: string, value: string) {
    this._storageType.setItem(key, value);
  }

  public get(key: string) {
    this._storageType.getItem(key);
  }

  public remove(key: string) {
    this._storageType.removeItem(key);
  }
}
