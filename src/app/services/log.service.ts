import { Injectable } from '@angular/core';

@Injectable()
export class LogService {

  constructor() { }

  public message(msg: string) {
    console.log(msg);
  }

  public error(msg: string) {
    console.error(msg);
  }
}
