import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { APP_SETTINGS } from './../settings';
import { Title, Meta } from '@angular/platform-browser';

@Injectable()
export class AppService {

  constructor(private _title: Title, private _meta: Meta, private _router: Router) { }

  public setPageMetaFromRouteData() {
    const title = this.getTitle(this._router.routerState, this._router.routerState.root).join(' - ');
    this._title.setTitle(title || APP_SETTINGS.DEFAULT_TITLE);
    const meta = this.getMeta(this._router.routerState, this._router.routerState.root);

    if (meta && meta.length) {
      for (const metaItem of meta[0]) {
        const currentMetaTag = this._meta.getTag(`name="${metaItem.name}"`);
        if (currentMetaTag) {
          this._meta.updateTag(metaItem);
        } else {
          this._meta.addTag(metaItem);
        }
      }
    }
  }

  private getTitle(state, parent) {
    const data = [];
    if (parent &&
      parent.snapshot.data &&
      parent.snapshot.data.page &&
      parent.snapshot.data.page.title) {
      data.push(parent.snapshot.data.page.title);
    }

    if (state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }

    return data;
  }

  private getMeta(state, parent) {
    const data = [];
    if (parent &&
        parent.snapshot.data &&
        parent.snapshot.data.page &&
        parent.snapshot.data.page.meta &&
        parent.snapshot.data.page.meta.length) {
      data.push(parent.snapshot.data.page.meta);
    }

    if (state && parent) {
      data.push(... this.getMeta(state, state.firstChild(parent)));
    }

    return data;
  }
}
