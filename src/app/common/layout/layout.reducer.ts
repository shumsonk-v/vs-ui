import * as layout from './layout.actions';

// State interface
export interface State {
  isSidenavOpened: boolean;
}

// Default state value
const initialState: State = {
  isSidenavOpened: false
};

// Export reducer
export function reducer(state: any = initialState, action: layout.LayoutActions): State {
  switch (action.type) {
    case layout.LayoutActionTypes.TOGGLE_SIDENAV: {
      const sidenavState = action.payload;
      return Object.assign({}, state, {
        isSidenavOpened: sidenavState
      });
    }

    default: return state;
  }
}

// Export function so that another classes can get state easily
export const getSidenavStatus = (state: State) => state.isSidenavOpened;
