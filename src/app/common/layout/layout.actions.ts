import { Action } from '@ngrx/store';

/* Action Types */
export const LayoutActionTypes = {
  TOGGLE_SIDENAV: 'TOGGLE_SIDENAV'
};

/* Action Classes (same as action function in React/Redux) */
export class ToggleSidenavAction implements Action {
  type = LayoutActionTypes.TOGGLE_SIDENAV;

  // payload is a value that will be sent to reducer along with type
  constructor(public payload: boolean) {}
}

/* Export type so that the reducer can use it. Export multiple type by putting | (pipe) between the action classes */
export type LayoutActions = ToggleSidenavAction;
