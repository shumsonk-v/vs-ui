import { createSelector } from 'reselect';
import { storeLogger } from 'ngrx-store-logger';
import { compose } from '@ngrx/core';
import { combineReducers, ActionReducer, MetaReducer, ActionReducerMap } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import * as fromRouter from '@ngrx/router-store';
import { Params } from '@angular/router';
import { environment } from '../../environments/environment';
import * as fromLayout from './layout/layout.reducer';

// Interface for application state
export interface AppState {
  reducer: {
    layout: fromLayout.State;
  };
}

// Interface for (Angular) router state url
export interface RouterStateUrl {
  url: string;
  queryParams: Params;
}

// Interface for state combining reducers
export interface State {
  layout: fromLayout.State;
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}

// Application reducers
export const reducers: ActionReducerMap<State> = {
  layout: fromLayout.reducer,
  routerReducer: fromRouter.routerReducer,
};

// Reducer logger function
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function(state: State, action: any): State {
    // === Disabled log ===
    // console.log('state', state);
    // console.log('action', action);

    return reducer(state, action);
  };
}

// Set metadata for reducer. Use log function in dev mode
export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [logger, storeFreeze]
  : [];

// Export utility function so it can be used easily to get the state.
export const getLayoutState = (state: AppState) => state.reducer.layout;
export const getLayoutSidenavState = createSelector(getLayoutState, fromLayout.getSidenavStatus);
