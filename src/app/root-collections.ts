import { HomeComponent } from './components/pages/home/home.component';
import { AppErrorComponent } from './components/system/app-error/app-error.component';
import { AppLoaderComponent } from './components/system/app-loader/app-loader.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { AppService } from './services/app.service';
import { AuthenticatedGuard } from './guards/auth.guard';
import { StorageService } from './services/storage.service';
import { AboutComponent } from './components/pages/about/about.component';


export const COMPONENTS = [
  HomeComponent,
  AppErrorComponent,
  AppLoaderComponent,
  SidenavComponent,
  AboutComponent
];

export const SERVICES = [
  AppService,
  StorageService
];

export const GUARDS = [
  AuthenticatedGuard
];
