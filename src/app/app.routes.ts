import { Routes } from '@angular/router';
import { AppErrorComponent } from './components/system/app-error/app-error.component';
import { HomeComponent } from './components/pages/home/home.component';
import { AuthenticatedGuard } from './guards/auth.guard';
import { AboutComponent } from './components/pages/about/about.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      page: {
        title: '',
        meta: []
      }
    }
  },
  {
    path: 'guideline',
    loadChildren: './components/pages/guideline#GuidelineModule'
  },
  {
    path: 'about',
    component: AboutComponent,
    data: {
      page: {
        title: '',
        meta: []
      }
    }
  },

  // == PROTECTED ROUTES ===
  {
    path: 'admin',
    component: HomeComponent,
    canActivate: [AuthenticatedGuard],
    data: {
      page: {
        title: '',
        meta: []
      }
    }
  },

  // === ERROR PAGE ROUTES ===
  // All error pages must be redirected to AppErrorComponent and parse the data as their error contents
  // The content will be in 'error' property (object type) with following sub properties
  // - code (number): Error code
  // - title (string): Error message title
  // - description (string): Error message detail
  // All properties are optional. If you don't specify, it just doesn't display it.
  {
    path: 'unauthorized',
    component: AppErrorComponent,
    data: {
      page: {
        title: 'Unauthorized'
      },
      error: {
        code: 403,
        title: 'Unauthorized',
        description: 'This is a restricted area and looks like you\'re not allowed to access it'
      }
    }
  },
  {
    path: '**',
    component: AppErrorComponent,
    data: {
      page: {
        title: 'Page Not Found'
      },
      error: {
        code: 404,
        title: 'Page Not Found',
        description: 'We can\'t seem to find the page you\'re looking for'
      }
    }
  }
];
